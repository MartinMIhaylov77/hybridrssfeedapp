﻿using System.ComponentModel.DataAnnotations;
namespace HybridRSSFeedApp.Website.DTOs
{
    public class LoginDTO
    {
        [Required]
        public string Email { get; set; } = "";

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; } = "";
    }
}

using HybridRSSFeedApp.Components.Components.Pages;
using HybridRSSFeedApp.Components.Data;
using HybridRSSFeedApp.Components.Services;
using HybridRSSFeedApp.Website.Components;
using HybridRSSFeedApp.Website.Data;
using HybridRSSFeedApp.Website.Models;
using HybridRSSFeedApp.Website.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Maui.Networking;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddRazorComponents()
    .AddInteractiveServerComponents();

AppProps.AppType = AppType.Blazor;

builder.Services.AddHttpClient("custom-httpclient", httpClient =>
{
    var baseAddress = "http://localhost:5099";
    httpClient.BaseAddress = new Uri(baseAddress);
});

builder.Services.AddSingleton<RssFeedService>();
builder.Services.AddSingleton<IConnectivity,
    BlazorConnectivity>();

builder.Services.AddDbContext<AppDbContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddAuthentication(options =>
{
    options.DefaultScheme = IdentityConstants.ApplicationScheme;
    options.DefaultSignInScheme = IdentityConstants.ExternalScheme;
}).AddIdentityCookies();

builder.Services.AddScoped<IAccount, Account>();

builder.Services.AddIdentityCore<ApplicationUser>().AddRoles<IdentityRole>().AddEntityFrameworkStores<AppDbContext>().AddSignInManager().AddDefaultTokenProviders();

var app = builder.Build();

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error", createScopeForErrors: true);

    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();
app.UseAntiforgery();
app.MapRazorComponents<App>()
    .AddInteractiveServerRenderMode()
    .AddAdditionalAssemblies(typeof(RssFeed).Assembly);

app.Run();

public class BlazorConnectivity : IConnectivity
{
    public IEnumerable<ConnectionProfile> ConnectionProfiles => null;

    public NetworkAccess NetworkAccess => NetworkAccess.Internet;

    public event EventHandler<ConnectivityChangedEventArgs> ConnectivityChanged;
}

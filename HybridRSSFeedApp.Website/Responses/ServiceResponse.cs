﻿namespace HybridRSSFeedApp.Website.Responses
{
    public record  ServiceResponse(bool Flag, string Message = null!);
}

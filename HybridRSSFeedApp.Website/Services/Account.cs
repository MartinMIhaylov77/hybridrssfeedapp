﻿using HybridRSSFeedApp.Website.DTOs;
using HybridRSSFeedApp.Website.Models;
using HybridRSSFeedApp.Website.Responses;
using Microsoft.AspNetCore.Identity;

namespace HybridRSSFeedApp.Website.Services
{
    public class Account : IAccount
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly RoleManager<IdentityRole> roleManager;

        public Account(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, RoleManager<IdentityRole> roleManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
        }

        public async Task<ServiceResponse> RegisterAsync(RegisterDTO RegisterDTO)
        {
            var findUser = userManager.FindByEmailAsync(RegisterDTO.Email);
            if (findUser != null) return new ServiceResponse(false, "Useer already exist");

            var appUser = new ApplicationUser()
            {
                Name = RegisterDTO.Name,
                UserName = RegisterDTO.Email,
                PasswordHash = RegisterDTO.Password,
                Email = RegisterDTO.Email
            };

            var createUser = await userManager.CreateAsync(appUser, RegisterDTO.Password);

            if (createUser.Succeeded)
            {
                var adminRole = await roleManager.FindByNameAsync("Admin");
                if (adminRole != null)
                {
                    await roleManager.CreateAsync(new IdentityRole("Admin"));
                    await userManager.AddToRoleAsync(appUser!, ("Admin"));
                }
                else
                {
                    var userRole = await roleManager.FindByNameAsync("User");
                    if (userRole is null)
                        await roleManager.CreateAsync(new IdentityRole("Admin"));

                    await userManager.AddToRoleAsync(appUser!, ("User"));
                }

                return new ServiceResponse(true, "Your acount was created!");             
            }

            return new ServiceResponse(false, "Please try again error occured!");
        }

        public Task<ServiceResponse> LoginAsync(LoginDTO model)
        {
            throw new NotImplementedException();    
        }
    }
}

﻿using HybridRSSFeedApp.Website.DTOs;
using HybridRSSFeedApp.Website.Responses;

namespace HybridRSSFeedApp.Website.Services
{
    public interface IAccount
    {
        Task<ServiceResponse> RegisterAsync(RegisterDTO model);
        Task<ServiceResponse> LoginAsync(LoginDTO model);

    }
}

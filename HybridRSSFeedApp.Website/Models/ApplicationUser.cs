﻿using Microsoft.AspNetCore.Identity;

namespace HybridRSSFeedApp.Website.Models
{
    public class ApplicationUser : IdentityUser
    {

        public string Name { get; set; } = string.Empty;

    }
}

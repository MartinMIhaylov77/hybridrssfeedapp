
# HybridRSSFeedApp

Welcome to the simple RSSFeedHybridApp!

The aim of this app is to showcase the powerful capabilities provided by .NET MAUI and Blazor for developers. With this app, I've built an entire Blazor Hybrid Application powered by .NET MAUI, sharing components, pages, and business logic with a full Blazor Web App. The RSSFeedHybridApp allows us to write code that can be shared across multiple platforms: Windows, Android, Web, iOS, and more.

This app serves as a sample application to fetch RSS feeds from your desired URLs. The main goal is to demonstrate how we can share single components, pages, and services between three platforms


## Features

- Cross-Platform Development: Write once, run anywhere. The app showcases the ability to share code across different platforms seamlessly.


![SharredComponentRSS](/uploads/fb8bd1057853ae6ceeb8e304c2e68ac6/SharredComponentRSS.png)

The Components, Pages, and Services shown in the picture above are shared simultaneously between HybridRssFeedApp.Website (Blazor) and HybridRssFeedApp (.NET MAUI).
- RSS Feed Fetching: Users can fetch and display RSS feeds from their preferred URLs.
- Authentication and Authorization: 

  .NET MAUI Project: Implemented using the OAuth framework and tokens via the latest .NET 8 technologies for a fast and     straightforward implementation.

![OAuthAPIEndPoints](/uploads/aadc37e272d466e7f1b4b26e98aa91c8/OAuthAPIEndPoints.png)


  Blazor Website: Utilizing cookie reclaiming technique for authentication and authorization.
  
![CookiesAuth](/uploads/4b0f17fa72b6285df0e093d00993cd3f/CookiesAuth.png)

- Local and Database Storage: Data is saved locally on disk or in a database.
- Integration of Native Components: Blending native components within the MAUI application.
- Scalable Backend: The RSSFeedAPI encapsulates the backend logic, enhancing the scalability of the services and allowing future migrations or integration with different frontend technologies.


## Implementation Details
- Visual Studio 2022: The app is built using Visual Studio 2022, leveraging its powerful features for .NET development.
- Shared Business Logic: Business logic is shared between the MAUI app and the Blazor Web App, ensuring consistency and reducing redundancy.
- Component Sharing: Components and pages are shared across platforms, demonstrating the flexibility and reusability of Blazor components.
- Backend API: The RSSFeedAPI handles backend logic, promoting scalability and ease of maintenance.
## Feature Enhancements
- Extend Functionality: Additional features can be added to further demonstrate the capabilities of .NET MAUI and Blazor.
- Enhanced Scalability: The backend services are designed to support future scalability, allowing for easy migration to other frontend technologies.
- Custom Render Modes: Specific attributes can be added for the Blazor Web App to control the rendering mode of pages.





## Deployment

To deploy this project run

- Clone from repository.
- Load by Visual Studio 2022
- Set a multiple Startup projects 
- Select RSSFeedAPI with HybridRssFeedApp(Desktop and Android platforms) or RssFeedAPI with HybridRSSFeedApp.Website(Web Application)

With the RSSFeedHybridApp, you can explore the seamless integration of .NET MAUI and Blazor, and see firsthand how shared components and business logic can simplify and enhance cross-platform development.

Thank you for exploring the RSSFeedHybridApp. Enjoy your experience, and feel free to contribute or extend the app as you see fit!



![Hello.net](/uploads/6296e1f43b59a69f9253a439919c8b6c/Hello.net.png)

![Shared1](/uploads/6711b38ee4a18bb892c15b50c4585ec9/Shared1.png)

![Shared2](/uploads/ba52be16e70eb5e6cb2847aae8b8fdb5/Shared2.png)


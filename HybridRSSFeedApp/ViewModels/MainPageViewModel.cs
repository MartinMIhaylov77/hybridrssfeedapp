﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HybridRSSFeedApp.Models;
using HybridRSSFeedApp.Services;
using HybridRSSFeedApp.Views;
using System.Text.Json;
namespace HybridRSSFeedApp.ViewModels
{
    public partial class MainPageViewModel : ObservableObject
    {
        [ObservableProperty]
        private RegisterModel registerModel;
        [ObservableProperty]
        private LoginModel loginModel;

        [ObservableProperty]
        private string userName;
        [ObservableProperty]
        private bool isAuthenticated;

        private bool isNotAuthenticated;
        public bool IsNotAuthenticated
        {
            get => !IsAuthenticated;
            set
            {
                SetProperty(ref isNotAuthenticated, value);
            }
        }

        private readonly AuthService clientService;
        public MainPageViewModel(AuthService clientService)
        {
            this.clientService = clientService;
            RegisterModel = new();
            LoginModel = new();
            IsAuthenticated = false;
            GetUserNameFromSecuredStorage();
        }

        [RelayCommand]
        private async Task Register()
        {
            await clientService.Register(RegisterModel);
            /* await Shell.Current.GoToAsync(nameof(Page))*/
            ;
            UpdateAuthenticationStatus();
        }

        [RelayCommand]
        private async Task Login()
        {
            await clientService.Login(LoginModel);
            GetUserNameFromSecuredStorage();
            //await Shell.Current.GoToAsync(nameof(Page));
            UpdateAuthenticationStatus();
        }

        [RelayCommand]
        private async Task Logout()
        {
            SecureStorage.Default.Remove("Authentication");
            IsAuthenticated = false;
            UserName = "Guest";
            await Shell.Current.GoToAsync("..");
            UpdateAuthenticationStatus();
        }

        private async void GetUserNameFromSecuredStorage()
        {
            if (!string.IsNullOrEmpty(UserName) && userName! != "Guest")
            {
                IsAuthenticated = true;
                return;
            }
            var serializedLoginResponseInStorage = await SecureStorage.Default.GetAsync("Authentication");
            if (serializedLoginResponseInStorage != null)
            {
                UserName = JsonSerializer.Deserialize<LoginResponse>(serializedLoginResponseInStorage)!.UserName!;
                IsAuthenticated = true;

            }
            else
            {
                UserName = "Guest";
                IsAuthenticated = false;
            }

            UpdateAuthenticationStatus();
        }


        [RelayCommand]
        private async Task GoToRssFeedPage()
        {
            await Shell.Current.GoToAsync(nameof(RssFeedPage));
        }

        private void UpdateAuthenticationStatus()
        {
            OnPropertyChanged(nameof(IsAuthenticated));
            OnPropertyChanged(nameof(IsNotAuthenticated));
        }
    }
}

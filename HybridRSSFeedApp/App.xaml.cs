﻿namespace HybridRSSFeedApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new AppShell();
        }
        protected override Window CreateWindow(IActivationState activationState)
        {
            var window = base.CreateWindow(activationState);

            // Get the primary screen dimensions
            var displayInfo = DeviceDisplay.MainDisplayInfo;
            double screenHeight = displayInfo.Height / displayInfo.Density;
            double screenWidth = displayInfo.Width / displayInfo.Density;

            // Set the window size
            window.Height = screenHeight * 0.8; // Adjust as needed
            window.Width = screenWidth * 0.8;  // Adjust as needed

            return window;
        }
    }
}

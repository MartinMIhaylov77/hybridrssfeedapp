using HybridRSSFeedApp.ViewModels;

namespace HybridRSSFeedApp.Views;

public partial class RssFeedPage : ContentPage
{
	public RssFeedPage(RssFeedPageViewModel viewModel)
    {
        InitializeComponent();
        BindingContext = viewModel;
    }
}
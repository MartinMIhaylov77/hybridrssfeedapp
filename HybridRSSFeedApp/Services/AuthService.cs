﻿using HybridRSSFeedApp.Models;
using System.Net.Http.Json;
using System.Text.Json;
namespace HybridRSSFeedApp.Services
{
    public class AuthService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public AuthService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task Register(RegisterModel model)
        {
            var httpClient = _httpClientFactory.CreateClient("custom-httpclient");
            var result = await httpClient.PostAsJsonAsync("/register", model);
            if (result.IsSuccessStatusCode)
            {
                await Shell.Current.DisplayAlert("Alert", "Sucessfully Registered", "Ok");
            }
            else
            {
                await Shell.Current.DisplayAlert("Alert", result.ReasonPhrase, "Email or password are invalid. Passwords must have at least one non alphanumeric character, least one digit ('0'-'9') and at least one uppercase ('A'-'Z') ");
            }
        }

        public async Task Login(LoginModel model)
        {
            var httpClient = _httpClientFactory.CreateClient("custom-httpclient");
            var result = await httpClient.PostAsJsonAsync("/login", model);
            var response = await result.Content.ReadFromJsonAsync<LoginResponse>();
            if (response.AccessToken is not null)
            {
                var serializeResponse = JsonSerializer.Serialize(
                    new LoginResponse() { AccessToken = response.AccessToken, RefreshToken = response.RefreshToken, UserName = model.Email });
                await SecureStorage.Default.SetAsync("Authentication", serializeResponse);
            }
            else
            {
                await Shell.Current.DisplayAlert("Alert", result.ReasonPhrase, "Try again");
            }
        }
    }
}
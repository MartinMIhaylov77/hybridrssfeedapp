using HybridRSSFeedApp.Views;

namespace HybridRSSFeedApp;

public partial class AppShell : Shell
{
    public AppShell()
    {
        InitializeComponent();
        Routing.RegisterRoute(nameof(RssFeedPage), typeof(RssFeedPage));
    }
}
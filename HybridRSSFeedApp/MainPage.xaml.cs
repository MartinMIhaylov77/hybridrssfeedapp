﻿using HybridRSSFeedApp.ViewModels;
using HybridRSSFeedApp.Views;

namespace HybridRSSFeedApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage(MainPageViewModel mainPageViewModel)
        {
            InitializeComponent();
            BindingContext = mainPageViewModel;
        }
        private async void Button_Clicked(object sender, EventArgs e)
        {

            await Shell.Current.GoToAsync(nameof(RssFeedPage));
        }

        //private async void Button_Clicked(object sender, EventArgs e)
        //{
        //    if (sender is Button button)
        //    {
        //        if (button.Text == "Register" || button.Text == "Login")
        //        {
        //            //await Shell.Current.GoToAsync("//HomePage");
        //            await Navigation.PushModalAsync(new HomePage());
        //        }
        //    }
        //}
    }
}

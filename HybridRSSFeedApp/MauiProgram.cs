﻿using HybridRSSFeedApp.Components.Data;
using HybridRSSFeedApp.Components.Services;
using HybridRSSFeedApp.Services;
using HybridRSSFeedApp.ViewModels;
using HybridRSSFeedApp.Views;

namespace HybridRSSFeedApp
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                });
            builder.Services.AddMauiBlazorWebView();
#if ANDROID
            builder.Services.AddSingleton<IPlatformHttpMessageHandler, AndroidHttpMessageHandler>();
#else
            builder.Services.AddSingleton<IPlatformHttpMessageHandler, DefaultHttpMessageHandler>(); // Create a default or other platform-specific implementation if necessary
#endif

            builder.Services.AddHttpClient("custom-httpclient", httpClient =>
            {
                var baseAddress = DeviceInfo.Platform == DevicePlatform.Android ? "https://10.0.2.2:7286" : "https://localhost:7286";
                httpClient.BaseAddress = new Uri(baseAddress);
            }).ConfigureHttpMessageHandlerBuilder(configBuilder =>
            {
                var platformMessageHandler = configBuilder.Services.GetRequiredService<IPlatformHttpMessageHandler>();
                configBuilder.PrimaryHandler = platformMessageHandler.GetHttpMessageHandler();
            });

            builder.Services.AddSingleton<AuthService>();
            builder.Services.AddSingleton<MainPage>();
            builder.Services.AddSingleton<RssFeedPageViewModel>();
            builder.Services.AddSingleton<RssFeedPage>();
            builder.Services.AddSingleton<MainPageViewModel>();

            AppProps.AppType = AppType.Maui; // Set the AppType

            builder.Services.AddSingleton<RssFeedService>();
            builder.Services.AddSingleton<IConnectivity>(c =>
            Connectivity.Current);

            return builder.Build();
        }

    }
}

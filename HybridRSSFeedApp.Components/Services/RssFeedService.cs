﻿using HybridRSSFeedApp.Components.Data;
using Newtonsoft.Json;
namespace HybridRSSFeedApp.Components.Services
{
    public class RssFeedService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public RssFeedService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<List<RssFeedItem>> GetRssFeedAsync(string rssFeedUrl)
        {
            if (!Uri.TryCreate(rssFeedUrl, UriKind.Absolute, out var validatedUri) ||
               (validatedUri.Scheme != Uri.UriSchemeHttp && validatedUri.Scheme != Uri.UriSchemeHttps))
            {
                Console.WriteLine("Invalid URL provided.");
                return new List<RssFeedItem>();
            }
            try
            {
                var httpClient = _httpClientFactory.CreateClient("custom-httpclient");
                var response = await httpClient.GetAsync($"api/RssFeed?url={Uri.EscapeDataString(rssFeedUrl)}");

                response.EnsureSuccessStatusCode();

                var jsonString = await response.Content.ReadAsStringAsync();
                var rssFeedItems = JsonConvert.DeserializeObject<List<RssFeedItem>>(jsonString);

                return rssFeedItems;

            }
            catch (HttpRequestException httpEx)
            {
                Console.WriteLine($"HTTP Request Error: {httpEx.Message}");

                return null!;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"General Error: {ex.Message}");

                return null!;
            }
        }
    }
}

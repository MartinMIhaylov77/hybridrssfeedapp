﻿namespace HybridRSSFeedApp.Components.Data
{
    public enum AppType { Unknown, Blazor, Maui }

    public static class AppProps
    {
        public static AppType AppType { get; set; } = AppType.Unknown;
    }
}

﻿namespace HybridRSSFeedApp.Components.Data
{
    public class RssFeedItem
    {
        public string? Title { get; set; } = string.Empty;
        public string? Link { get; set; }
        public string? Description { get; set; }
        public DateTime PublishDate { get; set; }
    }
}

﻿using System.Text.RegularExpressions;
namespace RSSFeedAPI.Models
{
    public class RssFeedItem
    {
        private string? _title;
        private string? _link;
        private string? _description;

        public string? Title
        {
            get => _title;
            set => _title = value != null ? RemoveHtmlTags(value) : null;
        }

        public string? Link
        {
            get => _link;
            set => _link = value != null ? RemoveHtmlTags(value) : null;
        }

        public string? Description
        {
            get => _description;
            set => _description = value != null ? RemoveHtmlTags(value) : null;
        }

        public DateTime? PublishDate { get; set; }

        private string RemoveHtmlTags(string? htmlContent)
        {
            if (string.IsNullOrEmpty(htmlContent))
            {
                return htmlContent ?? string.Empty;
            }

            return Regex.Replace(htmlContent, @"<[^>]+>|&nbsp;", "").Trim();
        }
    }
}
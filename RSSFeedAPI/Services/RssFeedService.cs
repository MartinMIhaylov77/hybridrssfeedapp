﻿using CodeHollow.FeedReader;
using RSSFeedAPI.Models;
using System.Reflection.PortableExecutable;
using System.ServiceModel.Syndication;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;

namespace RSSFeedAPI.Services
{
    public class RssFeedService
    {
        private readonly HttpClient _httpClient;
        public RssFeedService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<List<RssFeedItem>> GetFeedItemsAsync(string rssFeedUrl)
        {
            try
            {
                var feed = await FeedReader.ReadAsync(rssFeedUrl);

                var rssFeedItems = new List<RssFeedItem>();

                foreach (var item in feed.Items)
                {
                    var cleanedDescription = RemoveHtmlTags(item.Description);
  
                    var rssItem = new RssFeedItem
                    {
                        Title = item.Title,
                        Link = item.Link,
                        Description = item.Description,
                        PublishDate = item.PublishingDate
                      
                    };

                    rssFeedItems.Add(rssItem);
                }

                return rssFeedItems;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error fetching or parsing RSS feed: {ex.Message}");
                return new List<RssFeedItem>();
            }
        }
        private string RemoveHtmlTags(string htmlContent)
        {
            if (string.IsNullOrEmpty(htmlContent))
            {
                return htmlContent;
            }

            return Regex.Replace(htmlContent, @"<[^>]+>|&nbsp;", "").Trim();
        }
    }
}


﻿using Microsoft.AspNetCore.Mvc;
using RSSFeedAPI.Models;
using RSSFeedAPI.Services;
namespace RSSFeedAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RssFeedController : ControllerBase
    {
        private readonly RssFeedService _rssFeedService;
        public RssFeedController(RssFeedService rssFeedService)
        {
            _rssFeedService = rssFeedService;
        }

        [HttpGet]
        public async Task<IActionResult> GetRssFeed(string url)
        {
            try
            {
                List<RssFeedItem> rssFeedItems = await _rssFeedService.GetFeedItemsAsync(url);

                if (rssFeedItems == null || !rssFeedItems.Any())
                {
                    return NotFound("No RSS feed items found.");
                }

                return Ok(rssFeedItems);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error getting RSS feed: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}

